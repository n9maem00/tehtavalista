<?php require_once 'inc/top.php'; ?>
<h3>Tallenna</h3>
    <?php
        try{
            $kuvaus = filter_input(INPUT_POST, 'tehtava', FILTER_SANITIZE_STRING);
            $query = $db->prepare("INSERT INTO tehtava (kuvaus) VALUES (:kuvaus);");
            $query->bindValue(':kuvaus',$kuvaus,PDO::PARAM_STR);
            $query->execute();
            print "<p>Tehtävä lisätty!</p>";
        }
        catch(PDOException $pdoex){
            print "<p>Tietojen tallennus epäonnistui. " . $pdoex->getMessage() . "</p>";
        }
    ?>
    <a href="index.php">Tehtävälistaan</a>
<?php require_once 'inc/bottom.php'; ?>